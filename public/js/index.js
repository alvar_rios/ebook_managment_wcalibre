$(document).ready(function() {
	var xhr=null;
	 $('[href^=#]').click(function (e) {
	      e.preventDefault();
	  });
	 
	 $('[href^=\\/book\\/delete]').click(function (e) {
		 if (!confirm("Are you sure you want to delete this book"))
		 {
			 e.preventDefault();
	 	 }
	  });
	 
	 $('#btn-add-book').click(function (e) {
		
		if( $('[name^=fileupload]').val().match('^.*\.(epub|EPUB|mobi|MOBI|azw3|AZW3|fb2|FB2)$')==null){
			alert("The book as an incorrect format");
			e.preventDefault();
		}
	  });
	 
	 $('#search').keyup(function(e){
		 if($.trim($('#search').val())==''){
			 $('#container-search').addClass("novisible");
			 $('#container').removeClass("novisible");
			 
		 }
		 if (e.which>='65' && e.which<='122'){
		 if (xhr != null){
			 xhr.abort();
		 }
		 
			  xhr = $.ajax({
				  url: "/book/search",
				  type: "post",
				  data: "search=" + $.trim($('#search').val()),
				  success: (function (dataCheck) {
					  var obj = dataCheck;
					  var length = obj.length;
					  $('#container').addClass("novisible");
					  $('#container-search').removeClass("novisible");
					  $('#container-search').html('');
					  for (var i = 0; i < length; i++) {
						  
						  str='<div id="element" class="span2">';
						  str=str +'<div class="span2"><a href="/book/showbookinfo/'+obj[i].id+'"><img src="/book/getbookthubnail/'+obj[i].id+'" class="img-rounded"></a></div>';
						  str=str +'<div class="span2"><a href="/book/showbookinfo/'+obj[i].id+'">'+obj[i].title+'</a></div>';
						  str=str +'<div class="span2">'+obj[i].author_sort+'</div>';
						  str=str +'</div>';
						  $('#container-search').append(str);
					  }
				  })
					  
					  
		              
				});
		 	
		 }
		 
	 });
	 
	
});