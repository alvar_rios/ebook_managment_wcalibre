<?php
namespace book\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\FileInput;
use Zend\Validator\File\Extension;
use Zend\Validator\File\Size;


class EditBookFormValidation
{
    public $id;
    public $title;
    public $synopsis;
    
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->id  = (isset($data['id']))  ? $data['id']     : null;
        $this->title  = (isset($data['title']))  ? $data['title']     : null; 
        $this->synopsis  = (isset($data['synopsis']))  ? $data['synopsis']     : null;
    } 
     
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
     
    public function getInputFilter($path)
    {
        
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
             
         
            
            $inputFilter->add(
                $factory->createInput(array(
                    'name'     => 'id',
                    'required' => true,
                    'filters'  => array(
                    		array('name' => 'Int'),
                    ),
                ))
            );
            
            $fileInput = new FileInput('cover');
            $fileInput->setRequired(false);
            $size = new Size(array('max'=>'20MB'));
            $fileExtension= new Extension(array('jpg'));
            $fileInput->getValidatorChain()->addValidator($size);
            $fileInput->getValidatorChain()->addValidator($fileExtension);
            $fileInput->getFilterChain()->attachByName(
            		'filerenameupload',
            		array(
            				'target'    => $path,
            				'randomize' => false,
            		        'overwrite' => true,
            		)
            );
            $inputFilter->add($fileInput);
            
            $inputFilter->add(
            		$factory->createInput(array
            		(
            				'name'     => 'title',
            				'required' => true,
            				'filters'  => array
            		        (
            						array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
            				),
            		        'validators' => array
            		        (
            		    		array
            		            (
            		    				'name'    => 'StringLength',
            		    				'options' => array
            		                            (
            		    						'encoding' => 'UTF-8',
            		    						'min'      => 1,
            		    						'max'      => 150,
            		    				         ),
            		            ),
            		       ),
            		)));
            $inputFilter->add(
            		$factory->createInput(array
            				(
            						'name'     => 'synopsis',
            						'required' => true,
            						'filters'  => array
            						(
            								array('name' => 'StripTags'),
            								array('name' => 'StringTrim'),
            						),
            						'validators' => array
            						(
            								array
            								(
            										'name'    => 'StringLength',
            										'options' => array
            										(
            												'encoding' => 'UTF-8',
            												'max'      => 10000,
            										),
            								),
            						),
            				)));
            
             
            $this->inputFilter = $inputFilter;
        }
         
        return $this->inputFilter;
    }
}
