<?php
namespace book\Model;



class Tag
{
	public $id;
	public $name;

	protected $inputFilter;

	public function exchangeArray($data)
	{
		$this->id = (isset($data['id'])) ? $data['id']: null;
		$this->name = (isset($data['name'])) ? $data['name']: null;

	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}