<?php
namespace book\Model;



class Series
{
	public $id;
	public $name;
	public $sort;
	protected $inputFilter;

	public function exchangeArray($data)
	{
		$this->id = (isset($data['id'])) ? $data['id']: null;
		$this->name = (isset($data['name'])) ? $data['name']: null;
		$this->sort = (isset($data['sort'])) ? $data['sort']: null;
	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}