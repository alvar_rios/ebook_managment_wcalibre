<?php
namespace book\Model;



class Comments
{
    public $id;
    public $book;
    public $text;
   
    

    public function exchangeArray($data)
    {
        $this->id = 	(isset($data['id']	)) ? $data['id']	: null;
        $this->book = 	(isset($data['book'])) ? $data['book']	: null;
        $this->text = 	(isset($data['text'])) ? $data['text']	: null;
        
    }

     public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
