<?php
namespace Book\Model;

use Zend\Db\TableGateway\TableGateway;



class TagIndexTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function getTagsIndex($id){
        $id  = (int) $id;
        $resultSet = $this->tableGateway->select(array('book' => $id));
        
       
        
        $tags=array();
        foreach ($resultSet as $row){
            
            $tags[]=$row->tag;  
        }
       
        return $tags;
    }
    
    
}