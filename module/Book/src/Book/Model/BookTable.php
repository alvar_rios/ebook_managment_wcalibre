<?php
namespace Book\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class BookTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
        //FUNCIONES DE SELECT
    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function fetchPagination($id){
    	
    	
    	$id=(int) $id*10;
    	
    	$select = new Select();
    	$select->from('books');
    	$select->limit(10);
    	$select->offset($id);
        
    	$resultSet = $this->tableGateway->selectWith($select);
    	return $resultSet;
    	
    }
    
    
    public function getTotalPages(){
        $sql_result = $this->tableGateway->getAdapter()->getDriver()->createStatement('select count(*) as num from books')->execute();
        $results = new ResultSet();
        
        
        
        $row = $results->initialize($sql_result)->current();
        $num=(int) $row->num;
        
        if (($num % 10) ==0){
        	$num=$num/10;
        }
        else{
        	$num=(int) ($num/10);
        	$num=$num+1;
        	
        }
        
       return $num;
        
    }

	public function getBook($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
    public function getBookPath($id)
    {
    	$id  = (int) $id;
    	$rowset = $this->tableGateway->select(array('id' => $id));
    	$row = $rowset->current();
    	if (!$row) {
    		throw new \Exception("Could not find row $id");
    	}
    	return $row->path;
    }
    
    public function getconsultalike($value){
        
        
       $where = new Where();
       $where->like('title', $value);
       
       $select = new Select();
       $select->from('books');
       $select ->where($where);
       $select->limit(10);
        
        $resultSet = $this->tableGateway->selectwith($select);
        return $resultSet;
        
    }
    //FUNCIONES DE INSERT
    public function insertbook($path){
    	exec('calibredb add  '. $path .' --with-library '. getcwd() .'/data/llibrescalibre/' ) ;
    }
    
    //FUNCIONES DE EDIT
    public function edittitle($id,$title){
        exec('calibre-debug -c "from calibre.library.database import LibraryDatabase; db = LibraryDatabase(\''. getcwd() .'/data/llibrescalibre/metadata.db\'); db.set_title('.$id.',\''.$title.'\');"');
    }
    //FUNCIONES DE DELETE
    public function deletebook($id){
        exec('calibredb remove  '. $id .' --with-library '. getcwd() .'/data/llibrescalibre/' ) ;
    }

    
}
