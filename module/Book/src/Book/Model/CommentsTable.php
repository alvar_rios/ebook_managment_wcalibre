<?php
namespace Book\Model;

use Zend\Db\TableGateway\TableGateway;
use book\Model\Comments;
use Zend\Db\ResultSet\ResultSet;

class CommentsTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

	public function getComment($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('book' => $id));
        $row = $rowset->current();
        
        if (!$row) {
        	
        	$row = new Comments();
        	$data=array('id' => '0','book'=>'0','text'=>'No synopsis');
        	$row->exchangeArray($data);
        	
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    }
        
    //FUNCIONES DE EDIT
    public function editcomment($book,$synopse){
        if ($this->getComment($book)->id!='0'){
            $this->tableGateway->update(array('text'=>$synopse),array('book'=>$book));
        }else{
            $sql_result = $this->tableGateway->getAdapter()->getDriver()->createStatement('select max(id) as num from comments')->execute();
            $results = new ResultSet();
            $row = $results->initialize($sql_result)->current();
            $id=(int) $row->num+1;
            $this->tableGateway->insert(array('id'=>$id,'book'=>$book,'text' => $synopse));
            
        }
    }
}
