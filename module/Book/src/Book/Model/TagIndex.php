<?php
namespace book\Model;



class TagIndex
{
	public $id;
	public $book;
	public $tag;

	protected $inputFilter;

	public function exchangeArray($data)
	{
		$this->id = (isset($data['id'])) ? $data['id']: null;
		$this->book = (isset($data['book'])) ? $data['book']: null;
		$this->tag = (isset($data['tag'])) ? $data['tag']: null;

	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}