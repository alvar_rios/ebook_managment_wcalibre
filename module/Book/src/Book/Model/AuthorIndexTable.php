<?php
namespace Book\Model;

use Zend\Db\TableGateway\TableGateway;



class AuthorIndexTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function getAuthorIndex($id){
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('book' => $id));
        $row = $rowset->current();
        return $row->author;
    }
    
    
}