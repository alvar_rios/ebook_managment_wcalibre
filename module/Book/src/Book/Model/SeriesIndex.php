<?php
namespace book\Model;



class SeriesIndex
{
	public $id;
	public $book;
	public $series;
	protected $inputFilter;

	public function exchangeArray($data)
	{
		$this->id = (isset($data['id'])) ? $data['id']: null;
		$this->book = (isset($data['book'])) ? $data['book']: null;
		$this->series = (isset($data['series'])) ? $data['series']: null;
	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}
