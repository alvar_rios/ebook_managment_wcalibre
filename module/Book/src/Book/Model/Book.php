<?php
namespace book\Model;



class Book
{
    public $id;
    public $author_sort;
    public $title;
    public $path;
    public $pubdate;
    public $series_index;
    protected $inputFilter; 

    public function exchangeArray($data)
    {
        $this->id = (isset($data['id'])) ? $data['id']: null;
        $this->author_sort = (isset($data['author_sort'])) ? $data['author_sort']: null;
        $this->title = (isset($data['title'])) ? $data['title']: null;
        $this->path = (isset($data['path'])) ? $data['path']: null;
        $this->pubdate = (isset($data['pubdate'])) ? $data['pubdate']: null;
        $this->series_index = (isset($data['series_index'])) ? $data['series_index']: null;
    }

     public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    public function toArray(){
    	$values= array();
    	$values['id']=$this->id;
    	$values['author_sort']=$this->author_sort;
    	$values['title']=$this->title;
    	$values['pubdate']=$this->pubdate;
    	$values['series_index']=$this->series_index;
    	
   
        return $values;
    }
}
