<?php
namespace book\Model;



class AuthorIndex
{
	public $id;
	public $book;
	public $author;
	protected $inputFilter;

	public function exchangeArray($data)
	{
		$this->id = (isset($data['id'])) ? $data['id']: null;
		$this->book = (isset($data['book'])) ? $data['book']: null;
		$this->author = (isset($data['author'])) ? $data['author']: null;
	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
}
