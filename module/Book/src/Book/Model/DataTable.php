<?php
namespace Book\Model;

use Zend\Db\TableGateway\TableGateway;



class DataTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    

	
    
    public function getData($id){
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('book' => $id));
         
        return $rowset;
        
    }
    
    public function getDataFormat($id)
    {
    	$id  = (int) $id;
    	$rowset = $this->tableGateway->select(array('book' => $id));
    	$row = $rowset->current();
    	if (!$row) {
    		throw new \Exception("Could not find row $id");
    	}
    	return $row->format;
    }
    
    public function getDataName($id,$format)
    {
    	$id  = (int) $id;
    	$rowset = $this->tableGateway->select(array('book' => $id,'format' => $format));
    	$row = $rowset->current();
    	if (!$row) {
    		throw new \Exception("Could not find row $id");
    	}
    	return $row->name;
    }
    
    
    
}
