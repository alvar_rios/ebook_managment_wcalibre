<?php
namespace book\Model;



class Language
{
    public $id;
    public $lang_code;
   

    public function exchangeArray($data)
    {
        $this->id = 				(isset($data['id']					)) ? $data['id']				: null;
        $this->lang_code = 			(isset($data['lang_code']			)) ? $data['lang_code']			: null;
       
    }

     public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
