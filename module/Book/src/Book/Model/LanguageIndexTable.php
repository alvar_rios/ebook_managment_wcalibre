<?php
namespace Book\Model;

use Zend\Db\TableGateway\TableGateway;



class LanguageIndexTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function getLanguageIndex($id){
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('book' => $id));
        $row = $rowset->current();
        return $row->lang_code;
    }
    
    
}