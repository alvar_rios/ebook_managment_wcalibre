<?php
namespace book\Model;



class Data
{
    public $id;
    public $book;
    public $format;
    public $uncompresset_size;
    public $name; 

    public function exchangeArray($data)
    {
        $this->id = 				(isset($data['id']					)) ? $data['id']				: null;
        $this->book = 				(isset($data['book']				)) ? $data['book']				: null;
        $this->format = 			(isset($data['format']				)) ? $data['format']			: null;
        $this->uncompresset_size = 	(isset($data['uncompresset_size']	)) ? $data['uncompresset_size']	: null;
        $this->name = 				(isset($data['name']				)) ? $data['name']				: null;
    }

     public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
