<?php
namespace Book\Model;

use Zend\Db\TableGateway\TableGateway;

class TagTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function getTags($id){
        try{
            $resultset = $this->tableGateway->select(array('id' => $id));
        }catch(\PDOException $e){
            
        }
        return  $resultset;
    }
    
    
}