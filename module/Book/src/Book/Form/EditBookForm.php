<?php
namespace Book\Form;

use Zend\Form\Form;
use Zend\Form\Element\Textarea;

class EditBookForm extends Form
{
    public function __construct($id,$title,$synopse)
    {
        parent::__construct('book');
        $textarea=new Textarea('synopsis');
        $textarea->setAttribute('class', 'span7');
        $textarea->setAttribute('rows', '7');
        $textarea->setAttribute('value', $synopse);
        
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype','multipart/form-data');
        $this->setAttribute('accept-charset', 'utf-8');
       
        $this->add(array(
        		'name' => 'id',
        		'attributes' => array(
        				'type'  => 'hidden',
        				'value' => $id,
        		),));
        $this->add(array(
        		'name' => 'cover',
        		'attributes' => array(
        				'type'  => 'file',
        		),
        
        ));
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type'  => 'text',
                'value' => $title,
                'class' => 'span7',
                
            ),)); 
        $this->add($textarea
            );
         
    }
}
