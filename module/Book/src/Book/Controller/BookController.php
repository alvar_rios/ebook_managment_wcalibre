<?php
namespace Book\Controller;


use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


use Book\Model\FormUpload;
use Book\Model\EditBookFormValidation;

use Book\Form\BookForm;
use Book\Form\EditBookForm;

use Zend\Validator\File\Extension;
use Zend\Validator\File\Size;

use Zend\Json\Json;

class BookController extends AbstractActionController
{
    protected $bookTable;
    protected $commentsTable;
    protected $dataTable;
    protected $seriesindexTable;
    protected $seriesTable;
    protected $authorindexTable;
    protected $authorTable;
    protected $tagindexTable;
    protected $tagTable;
    protected $languageindexTable;
    protected $languageTable;
    
    
    ///////////////Actions
    
    /** Action inicial muestra los datos en un gridview paginado
     *  Posiblemente pendiente de modificar la view
     * 
     * (non-PHPdoc)
     * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
     */
    public function indexAction()
    {
    	$id   = (int) 	$this->params()->fromRoute('id', 0);
    	$pagination['numpag']=$id;
    	$id--;
    	
    	$books=$this->getBookTable()->fetchPagination($id);
    	$books->buffer();
    	$datas=$this->getDataTable()->fetchAll();
    	$datas->buffer();
    	$pagination['totalpag']=$this->getBookTable()->getTotalPages();
		return new ViewModel(array(
            'books' => $books,
			'datas'	=> $datas,
		    'pagination' => $pagination,
        ));
    }
   
    /** Action que se encarga de mostrar/processar un archivo ebook
     *  los tipos aceptados son epub,mobi,azw3,fb2
     *  
     *  Retorna el form, el form con errores, o te retorna al indice en caso de no dar error.
     * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>|multitype:\Book\Form\BookForm
     */
    public function addAction()
    {
    	$form = new BookForm();
    	$request = $this->getRequest();
    	
    	//si entramos por primera vez ense�amos form sino procesamos form
    	if ($request->isPost()) {
    		 
    		$formupload = new FormUpload();
    		$form->setInputFilter($formupload->getInputFilter());
    		$file    = $this->getRequest()->getFiles()->toArray();
    		$form->setData($file);
    	
    	 	 if ($form->isValid()) 
    		 {
    		     //anyadimos validaci�nes para el fichero
    		 	$size = new Size(array('max'=>'20MB'));
    		 	$fileExtension= new Extension(array('epub','mobi','azw3','fb2'));
    		 	
    		 	$adapter = new \Zend\File\Transfer\Adapter\Http();
    		 	$adapter->setValidators(array($size,$fileExtension), $file['name']);
    		 	if (!$adapter->isValid())
    		 	{
    		 		$dataError = $adapter->getMessages();
    		 		$error = array();
    		 		foreach($dataError as $key=>$row)
    		 		{
    		 			$error[] = $row;
    		 		} //set formElementErrors
    		 		$form->setMessages(array('fileupload'=>$error ));
    		 	} 
    		 	else 
    		 	{
    		 		
    		 		$adapter->setDestination( getcwd() .'/data/');
    		 		if ($adapter->receive($file['name'])) 
    		 		{
    		 			$formupload->exchangeArray($form->getData());
    		 			$path= $adapter->getFileName();
    		 			
    		 			$path=$this->editpath($path);
    		 			
    		 			$this->getBookTable()->insertbook($path);

    		 			unlink($adapter->getFileName());
    		 			
    		 			return $this->redirect()->toRoute('book');
    		 		}
    		 	}
    		 	
    		 }
    	}
    	
    	return array('form' => $form);
    }

    public function editAction()
    {
        
        
        $id   = (int) 	$this->params()->fromRoute('id', 0);
        $path =  getcwd() .'/data/llibrescalibre/'.$this->getBookTable()->getBookPath($id);
        $title= html_entity_decode($this->getBookTable()->getBook($id)->title,null,'UTF-8');
        $synopse=$this->getCommentsTable()->getComment($id)->text;
       
        
        $form = new EditBookForm($id,$title,$synopse);
        $request = $this->getRequest();
        
        //si entramos por primera vez ense�amos form sino procesamos form
        if ($request->isPost()) {
            $formeditval = new EditBookFormValidation();
            $form->setInputFilter($formeditval->getInputFilter($path.'/cover.jpg'));
            
            $post = array_merge_recursive(
            		$this->getRequest()->getPost()->toArray(),
            		$this->getRequest()->getFiles()->toArray()
            );
            
            $form->setData($post);
            if ($form->isValid())
            {
               
               $data=$form->getData(0);
                $idedit=$data['id'];
                $titleedit=$data['title'];
                $titleedit=htmlentities($titleedit,null,'UTF-8');
                $synopsisedit=$data['synopsis'];
                $this->getBookTable()->edittitle($idedit,$titleedit);
                $this->getCommentsTable()->editcomment($idedit,$synopsisedit);
                
                if (file_exists($path.'/thumbnail.jpg')){
                	unlink($path.'/thumbnail.jpg');
                }
                
                return $this->redirect()->toRoute('book',array('action'=> 'showbookinfo','id' => $id));
            	
            }
            
        }
       
        return array('form'      => $form,
                 
        );
    }
    
    /** Action que elimina un ebook
     * 
     * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>
     */
    public function deleteAction()
    {
    	$id   = (int) 	$this->params()->fromRoute('id', 0);
    	$this->getBookTable()->deletebook($id);
    	return $this->redirect()->toRoute('book');
    }
    
    /** Action que se encaga de descargar ficheros
     *  Si se envia mal el formato dara un throw de DataTable
     * 
     */
    public function downloadbookAction(){
    	$id   = (int) 	$this->params()->fromRoute('id', 0);
    	$type =			$this->params()->fromRoute('type', 0);
    	$path = 		$this->getBookTable()->getBookPath($id);
    	$name = 		$this->getDataTable()->getDataName($id,$type);
    	$file_dir= getcwd() ."/data/llibrescalibre/".$path."/";
    	
    	$type=strtolower($type);
    	
    	if (file_exists($file_dir.$name.'.'.$type)){
				$value=file_get_contents($file_dir.$name.'.'.$type);  
    	}
    	//preparacion de la view
    	$response = $this->getResponse();
    	$response->setContent($value);
    	$response
    	->getHeaders()
    	->addHeaderLine('Content-Disposition','filename="'.$name.'.'.$type.'"')
    	->addHeaderLine('Content-Type', 'application/octet-stream')
    	->addHeaderLine('Content-Length', mb_strlen($value));
    	 
    	//limpio buffer y envio la view(imagen)
    	ob_clean();
    	return $response;
    	
    }
    
    /**	Action que abre la imagen(thumbnail) de la portada del libro
     *  Si no existe la imagen selecciona un placeholder
     *  Si la imagen no tiene thumbnail lo crea
     * 
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function getbookthubnailAction(){
    	
    	
    	$id = (int) $this->params()->fromRoute('id', 0);
    	$path = $this->getBookTable()->getBookPath($id);
    
    	
    	//ruta completa de la imagen
    	$file_dir= getcwd() ."/data/llibrescalibre/".$path."/";
    	$file_name="cover.jpg";
    
    	
    	if (!file_exists($file_dir.$file_name))
    	{
    		$file_dir= getcwd() ."/data/llibrescalibre/";
    		$file_name="placeholder.jpg";
    	}
    	$imagine = $this->getServiceLocator()->get('my_image_service');
    	
    	if (!file_exists($file_dir.'thumbnail.jpg')){
    		$image   = $imagine->open($file_dir.$file_name);
    		$transformation = new \Imagine\Filter\Transformation();
    		$transformation	->thumbnail(new \Imagine\Image\Box(100, 150))
    						->save($file_dir.'thumbnail.jpg')
    						->apply($image);
    	}
    	
    	//preparacion de la view
    	$response = $this->getResponse();
    	$response->setContent($imagine->open($file_dir.'thumbnail.jpg')->get('jpg'));
    	$response
    	->getHeaders()
    	->addHeaderLine('Content-Type', 'image/jpg')
    	->addHeaderLine('Content-Length', mb_strlen($imagine->open($file_dir.'thumbnail.jpg')->get('jpg')));
    	
    	//limpio buffer y envio la view(imagen)
    	ob_clean();
    	return $response;
    	
    	
    	
    	
    }
    
    /**	Action que abre la imagen de la portada del libro
     *  Si no existe la imagen selecciona un placeholder
     *  Si la imagen no tiene thumbnail lo crea
     *
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function getbookcoverAction(){
    	 
    	 
    	$id = (int) $this->params()->fromRoute('id', 0);
    	$path = $this->getBookTable()->getBookPath($id);
    
    	 
    	//ruta completa de la imagen
    	$file_dir= getcwd() ."/data/llibrescalibre/".$path."/";
    	$file_name="cover.jpg";
    	 
    	if (!file_exists($file_dir.$file_name))
    	{
    		$file_dir= getcwd() ."/data/llibrescalibre/";
    		$file_name="placeholder.jpg";
    	}
    	$imagine = $this->getServiceLocator()->get('my_image_service');

    	//preparacion de la view
    	$response = $this->getResponse();
    	$response->setContent($imagine->open($file_dir.$file_name)->get('jpg'));
    	$response
    	->getHeaders()
    	->addHeaderLine('Content-Type', 'image/jpg')
    	->addHeaderLine('Content-Length', mb_strlen($imagine->open($file_dir.$file_name)->get('jpg')));
    	 
    	//limpio buffer y envio la view(imagen)
    	ob_clean();
    	return $response;
    }

    /** Action que se encarga de convertir un libro y anyadirlo
     *  a la bibliotecta
     * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>
     */
    public function convertbookAction(){
        $id         = (int) 	$this->params()->fromRoute('id', 0);
        $format     =			strtolower($this->params()->fromRoute('type', 0));
        $path       = 		    $this->getBookTable()->getBookPath($id);
        $path       =            getcwd() ."/data/llibrescalibre/".$this->editpath($path)."/";
        $format_ori =           strtolower($this->getDataTable()->getDataFormat($id));
        $name       =           $this->getDataTable()->getDataName($id,$format_ori);
        $name       =           $this->editpath($name);
        $ebook_convert='ebook-convert  '. $path.$name.".".$format_ori .' ' . $path.$name.".".$format;
        $calibre_add='calibredb add_format '.$id.' '. $path.$name.".".$format .' --with-library '. getcwd() .'/data/llibrescalibre/';
        
        exec($ebook_convert);
        exec( $calibre_add );
        return $this->redirect()->toRoute('book',array('action'=> 'showbookinfo','id' => $id));
        
        
    }
    
    /** Action que recoge todos los datos de UN libro
     *  y los envia a la view
     *  
     * @return \Zend\View\Model\ViewModel
     */
    public function showbookinfoAction()
    {
      
        
    	$id = (int) $this->params()->fromRoute('id', 0);
    	
    	$book=$this->getBookTable()->getBook($id);
    	$comments=$this->getCommentsTable()->getComment($id);
    	$datas=$this->getDataTable()->getData($id);
    	$series=$this->getSeriesTable()->getSerie($this->getSeriesIndexTable()->getSerieIndex($id));
    	$author=$this->getAuthorTable()->getAuthor($this->getAuthorIndexTable()->getAuthorIndex($id));
    	$tags=$this->getTagTable()->getTags($this->getTagIndexTable()->getTagsIndex($id));
    	$lang=$this->getLanguageTable()->getLanguage($this->getLanguageIndexTable()->getLanguageIndex($id));
	    	return new ViewModel(array(
	    			'comment'  => $comments,
	    	        'book'     => $book,
	    	        'datas'    => $datas,
	    	        'series'   => $series,
	                'author'   => $author,
	    	        'tags'     => $tags,
	    	        'lang'     => $lang,
	    	));
    	
    }
    
    public function searchaction(){
    	$request = $this->getRequest();
    	 
    	
    	if ($request->isPost()) {
    	   $values=$request->getPost()->toArray();
    	   
    	   $resultset=$this->getBookTable()->getconsultalike('%'.$values['search'].'%');
    	   
    	    $json = new Json();
    	    $jsonvalue = $json->encode($resultset);
    		$response = $this->getResponse();
    	    $response->setContent($jsonvalue);
    	    $response
    	    ->getHeaders()
    	    ->addHeaderLine('Content-Type', 'text/json');
    	    ob_clean();
    	    return $response;
    	}else{
    		return '';
    	}
    	
    }
    
    ///////////////Funciones
    /** Transforma path para que funcione bien en exec()
     *
     * @param String $path
     * @return mixed
     */
    public function editpath($path){
    	$path= str_replace(' ','\ ',$path);
    	$path= str_replace('(','\(',$path);
    	$path= str_replace(')','\)',$path);
    	$path= str_replace('{','\{',$path);
    	$path= str_replace('}','\}',$path);
    	$path= str_replace('&','\&',$path);
    	$path= str_replace('#','\#',$path);
    	$path= str_replace(';','\;',$path);
    	$path= str_replace('?','\?',$path);
    
    	return $path;
    }
    
    ///////////////Tablas
    public function getBookTable()
    {
        if (!$this->bookTable) {
            $sm = $this->getServiceLocator();
            $this->bookTable = $sm->get('Book\Model\BookTable');
        }
        return $this->bookTable;
    }
    
	public function getCommentsTable()
    {
       	if (!$this->commentsTable) {
    		$sm = $this->getServiceLocator();
    		$this->commentsTable = $sm->get('Book\Model\CommentsTable');
    	}
    	return $this->commentsTable;
    }
    
    public function getDataTable()
    {
    	if (!$this->dataTable) {
    		$sm = $this->getServiceLocator();
    		$this->dataTable = $sm->get('Book\Model\DataTable');
    	}
    	return $this->dataTable;
    }
    
    public function getSeriesIndexTable()
    {
    	if (!$this->seriesindexTable) {
    		$sm = $this->getServiceLocator();
    		$this->seriesindexTable = $sm->get('Book\Model\SeriesIndexTable');
    	}
    	return $this->seriesindexTable;
    }
    public function getSeriesTable()
    {
    	if (!$this->seriesTable) {
    		$sm = $this->getServiceLocator();
    		$this->seriesTable = $sm->get('Book\Model\SeriesTable');
    	}
    	return $this->seriesTable;
    }
    
    public function getAuthorIndexTable()
    {
    	if (!$this->authorindexTable) {
    		$sm = $this->getServiceLocator();
    		$this->authorindexTable = $sm->get('Book\Model\AuthorIndexTable');
    	}
    	return $this->authorindexTable;
    }
    public function getAuthorTable()
    {
    	if (!$this->authorTable) {
    		$sm = $this->getServiceLocator();
    		$this->authorTable = $sm->get('Book\Model\AuthorTable');
    	}
    	return $this->authorTable;
    }
    public function getTagIndexTable()
    {
    	if (!$this->tagindexTable) {
    		$sm = $this->getServiceLocator();
    		$this->tagindexTable = $sm->get('Book\Model\TagIndexTable');
    	}
    	return $this->tagindexTable;
    }
    public function getTagTable()
    {
    	if (!$this->tagTable) {
    		$sm = $this->getServiceLocator();
    		$this->tagTable = $sm->get('Book\Model\TagTable');
    	}
    	return $this->tagTable;
    }
    public function getLanguageIndexTable()
    {
    	if (!$this->languageindexTable) {
    		$sm = $this->getServiceLocator();
    		$this->languageindexTable = $sm->get('Book\Model\LanguageIndexTable');
    	}
    	return $this->languageindexTable;
    }
    public function getLanguageTable()
    {
    	if (!$this->languageTable) {
    		$sm = $this->getServiceLocator();
    		$this->languageTable = $sm->get('Book\Model\LanguageTable');
    	}
    	return $this->languageTable;
    }
    
}
