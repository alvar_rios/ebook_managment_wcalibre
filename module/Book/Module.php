<?php
namespace Book;

use Book\Model\Book;
use Book\Model\BookTable;

use Book\Model\Comments;
use Book\Model\CommentsTable;

use Book\Model\Data;
use Book\Model\DataTable;

use Book\Model\SeriesIndex;
use Book\Model\SeriesIndexTable;

use Book\Model\Series;
use Book\Model\SeriesTable;

use Book\Model\AuthorIndex;
use Book\Model\AuthorIndexTable;

use Book\Model\Author;
use Book\Model\AuthorTable;

use Book\Model\TagIndex;
use Book\Model\TagIndexTable;

use Book\Model\Tag;
use Book\Model\TagTable;

use Book\Model\LanguageIndex;
use Book\Model\LanguageIndexTable;

use Book\Model\Language;
use Book\Model\LanguageTable;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                   
                	
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Book\Model\BookTable' =>  function($sm) {
                    $tableGateway = $sm->get('BookTableGateway');
                    $table = new BookTable($tableGateway);
                    return $table;
                },
                'BookTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Book());
                    return new TableGateway('books', $dbAdapter, null, $resultSetPrototype);
                },
                'Book\Model\CommentsTable' =>  function($sm) {
                	$tableGateway = $sm->get('CommentsTableGateway');
                	$table = new CommentsTable($tableGateway);
                	return $table;
                },
                'CommentsTableGateway' => function ($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$resultSetPrototype = new ResultSet();
                	$resultSetPrototype->setArrayObjectPrototype(new Comments());
                	return new TableGateway('comments', $dbAdapter, null, $resultSetPrototype);
                },
                'Book\Model\DataTable' =>  function($sm) {
                	$tableGateway = $sm->get('DataTableGateway');
                	$table = new DataTable($tableGateway);
                	return $table;
                },
                'DataTableGateway' => function ($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$resultSetPrototype = new ResultSet();
                	$resultSetPrototype->setArrayObjectPrototype(new Data());
                	return new TableGateway('data', $dbAdapter, null, $resultSetPrototype);
                },
                'Book\Model\SeriesIndexTable' =>  function($sm) {
                	$tableGateway = $sm->get('SeriesIndexTableGateway');
                	$table = new SeriesIndexTable($tableGateway);
                	return $table;
                },
                'SeriesIndexTableGateway' => function ($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$resultSetPrototype = new ResultSet();
                	$resultSetPrototype->setArrayObjectPrototype(new SeriesIndex());
                	return new TableGateway('books_series_link', $dbAdapter, null, $resultSetPrototype);
                },
                'Book\Model\SeriesTable' =>  function($sm) {
                	$tableGateway = $sm->get('SeriesTableGateway');
                	$table = new SeriesTable($tableGateway);
                	return $table;
                },
                'SeriesTableGateway' => function ($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$resultSetPrototype = new ResultSet();
                	$resultSetPrototype->setArrayObjectPrototype(new Series());
                	return new TableGateway('series', $dbAdapter, null, $resultSetPrototype);
                },
                'Book\Model\AuthorIndexTable' =>  function($sm) {
                	$tableGateway = $sm->get('AuthorIndexTableGateway');
                	$table = new AuthorIndexTable($tableGateway);
                	return $table;
                },
                'AuthorIndexTableGateway' => function ($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$resultSetPrototype = new ResultSet();
                	$resultSetPrototype->setArrayObjectPrototype(new AuthorIndex());
                	return new TableGateway('books_authors_link', $dbAdapter, null, $resultSetPrototype);
                },
                'Book\Model\AuthorTable' =>  function($sm) {
                	$tableGateway = $sm->get('AuthorTableGateway');
                	$table = new AuthorTable($tableGateway);
                	return $table;
                },
                'AuthorTableGateway' => function ($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$resultSetPrototype = new ResultSet();
                	$resultSetPrototype->setArrayObjectPrototype(new Author());
                	return new TableGateway('authors', $dbAdapter, null, $resultSetPrototype);
                },
                'Book\Model\TagIndexTable' =>  function($sm) {
                	$tableGateway = $sm->get('TagIndexTableGateway');
                	$table = new TagIndexTable($tableGateway);
                	return $table;
                },
                'TagIndexTableGateway' => function ($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$resultSetPrototype = new ResultSet();
                	$resultSetPrototype->setArrayObjectPrototype(new TagIndex());
                	return new TableGateway('books_tags_link', $dbAdapter, null, $resultSetPrototype);
                },
                'Book\Model\TagTable' =>  function($sm) {
                	$tableGateway = $sm->get('TagTableGateway');
                	$table = new TagTable($tableGateway);
                	return $table;
                },
                'TagTableGateway' => function ($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$resultSetPrototype = new ResultSet();
                	$resultSetPrototype->setArrayObjectPrototype(new Tag());
                	return new TableGateway('tags', $dbAdapter, null, $resultSetPrototype);
                },
                'Book\Model\LanguageIndexTable' =>  function($sm) {
                	$tableGateway = $sm->get('LanguageIndexTableGateway');
                	$table = new LanguageIndexTable($tableGateway);
                	return $table;
                },
                'LanguageIndexTableGateway' => function ($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$resultSetPrototype = new ResultSet();
                	$resultSetPrototype->setArrayObjectPrototype(new LanguageIndex());
                	return new TableGateway('books_languages_link', $dbAdapter, null, $resultSetPrototype);
                },
                'Book\Model\LanguageTable' =>  function($sm) {
                	$tableGateway = $sm->get('LanguageTableGateway');
                	$table = new LanguageTable($tableGateway);
                	return $table;
                },
                'LanguageTableGateway' => function ($sm) {
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$resultSetPrototype = new ResultSet();
                	$resultSetPrototype->setArrayObjectPrototype(new Language());
                	return new TableGateway('languages', $dbAdapter, null, $resultSetPrototype);
                },
                
                
                
            ),
            'invokables' => array(
            		// defining it as invokable here, any factory will do too
            		'my_image_service' => 'Imagine\Gd\Imagine',
            ),
        );
    }
}
